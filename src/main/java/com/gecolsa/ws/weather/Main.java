package com.gecolsa.ws.weather;

import java.rmi.RemoteException;

import NET.webserviceX.www.GlobalWeatherSoapProxy;


public class Main {

	public static void main(String[] args) {
		GlobalWeatherSoapProxy client = new GlobalWeatherSoapProxy();
		try {
			String ciudades = client.getCitiesByCountry("Colombia");
			System.out.println(ciudades);
		} catch (RemoteException e) {
			System.out.println("NO SE PUDO CONECTAR CON EL SERVICIO!");
			e.printStackTrace();
		}
	}
}
